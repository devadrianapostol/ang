<?php
return [
    'adminEmail' => 'adrian.apostol86@gmail.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
