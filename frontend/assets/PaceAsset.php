<?php
namespace frontend\assets;


use yii\web\AssetBundle;
use yii\web\View;

class PaceAsset extends AssetBundle {
    public $js = [
        'js/pace.js'
    ];
    public $css = [
        'css/pace-theme-minimal.css'
    ];
    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
    public $cssOptions = [
        'position' => View::POS_HEAD,
    ];
}